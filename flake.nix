{
  outputs =
    { ... }:
    {
      templates = {
        default = {
          path = ./default;
          description = "Simple development environment";
        };
        haskell = {
          path = ./haskell;
          description = "Development environment for Haskell";
        };
        rust = {
          path = ./rust;
          description = "Development environment for Rust";
        };
      };
    };
}
