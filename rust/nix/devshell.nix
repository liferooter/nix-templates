{ ... }:
{
  perSystem = { pkgs, ...}: {
    devShells.default = pkgs.mkShell {
      packages = with pkgs; [
        rustc cargo
        clippy rustfmt
        rust-analyzer taplo
      ];
    };
  };
}
