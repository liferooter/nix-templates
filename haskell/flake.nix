{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    nix-commons.url = "gitlab:liferooter/nix-commons";
    stacklock2nix.url = "github:cdepillabout/stacklock2nix";
  };
  outputs = { flake-parts, ... }@inputs:
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        inputs.nix-commons.flakeModules.haskell
      ];

      perSystem.haskell = {
        pname = "<name>";
        ghcVersion = "<ghc version>";
      };

      systems = [ "x86_64-linux" ];
    };
}
